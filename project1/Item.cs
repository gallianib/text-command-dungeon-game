﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project1
{
    public class Item
    {
        public string iname = "";
        public double ivalue = 0.00;
        public string idescription = "";
        public int iquality = 0;
        public string itype = "";

        public Item(string n,double val,string desc, int quality, string type)
        {
            iname = n;
            ivalue = val;
            idescription = desc;
            iquality = quality;
            itype = type;

        }
        public Item()
        {

        }
        
       
    }
}
