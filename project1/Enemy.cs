﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace project1
{
    public class Enemy : Tile
    {
        public double hp = 100;
        public double attack = 1;
        public int lvl = 1;
        public double defense = 1;
        public double speed = 1;
        public bool alive = true;
        
        public Enemy()
        {
            IsEnemy = true;
        }
        public Enemy(int lvl)
        {
            Lvl = lvl;
            IsEnemy = true;
        }
        
        public bool Alive
        {
            get { return alive; }
            set { alive = value; }
        }
        
        public double HP
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
                if (hp <= 0) { Alive = false; }
            }
        }
        public double Attack
        {
            get
            {
                return Math.Abs((5 * Lvl)- (2.5*(Lvl + 1)))+1;
            }
            set
            {
                attack = value;
            }
        }

        public double Defense
        {
            get
            {
                return Math.Abs((3 * Lvl) - ((1.5 * Lvl) + 1.85));
            }

            set
            {
                defense = value;
            }
        }
        public int Lvl
        {
            get
            {
                return lvl;
            }
            set
            {
                lvl = value;
            }
        }
        public double Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = Lvl*2.345;
            }
        }

        public void Die(Character ch)
        {
            Random r = new Random();
            Item i = new Item();
            int randal = r.Next(0,1000);
            int randy = r.Next(1, 3);

            if (randal == 1)
            {
                if(randy == 1)
                {
                    i.iname = "Masterwork Steel Chestplate";
                    i.ivalue = 10000;
                    i.idescription = "Perfectly crafted Steel Chestplate";
                    i.itype = "armor";
                    i.iquality = 10;
                }
                else
                {
                    i.iname = "Masterwork Steel Sword";
                    i.ivalue = 10000;
                    i.idescription = "Perfectly Crafted Steel Sword";
                    i.iquality = 15;
                    i.itype = "weapon";
                }

            }
            else if (randal > 1 && randal <= 100)
            {
                if (randy == 1)
                {
                    i.iname = "Steel Chestplate";
                    i.ivalue = 1000;
                    i.idescription = "Sturdy Steel Chestplate";
                    i.itype = "armor";
                    i.iquality = 5;
                }
                else
                {
                    i.iname = "Steel Sword";
                    i.ivalue = 1000;
                    i.idescription = "Strong Steel Sword";
                    i.iquality = 9;
                    i.itype = "weapon";
                }
            }
            else if (randal > 100 && randal <= 250)
            {
                if (randy == 1)
                {
                    i.iname = "Iron Chainmail";
                    i.ivalue = 100;
                    i.idescription = "Iron Chain vest";
                    i.itype = "armor";
                    i.iquality = 3;
                }
                else
                {
                    i.iname = "Iron Sword";
                    i.ivalue = 100;
                    i.idescription = "Simple Iron Sword";
                    i.iquality = 5;
                    i.itype = "weapon";
                }
            }
            else if (randal > 250 && randal <= 500)
            {
                if (randy == 1)
                {
                    i.iname = "Leather Vest";
                    i.ivalue = 10;
                    i.idescription = "Shoddy Leather Vest";
                    i.itype = "armor";
                    i.iquality = 2;
                }
                else
                {
                    i.iname = "Wooden Sword";
                    i.ivalue = 10;
                    i.idescription = "Rough Wooden Sword";
                    i.iquality = 2;
                    i.itype = "weapon";
                }
            }
            else if (randal > 500 && randal <= 650)
            {
                i.iname = "Health Potion";
                i.ivalue = 100;
                i.idescription = "Heals a portion of Health. Can be used in combat. ('use potion')";
                i.itype = "potion";
                i.iquality = 1;
            }
            else
            {
                return;
            }
           
            ch.AddItemToInv(i);
                   
        }
    }
}
