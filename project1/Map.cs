﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project1
{
    public class Map
    {
        public Tile[,] map = new Tile[10, 10];
        Random r = new Random();
        public Character c = new Character(1);
        public Enemy e = new Enemy(11);
        public Map()
        {
        }
        public void initMap()
        {
            e.X = 9;
            e.Y = 9;
            c.Commands.Add("run");
            c.Commands.Add("attack");
            c.Commands.Add("rest");
            c.Commands.Add("help");
            c.Commands.Add("inv");
            c.Commands.Add("use");
            c.Commands.Add("stats");
            int randy = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == 0 && j == 0)
                    {
                        map[i, j] = c;
                    }
                    else
                    {
                        randy = r.Next(0, 4);
                        if (randy == 2)
                        {
                            map[i, j] = new Enemy(c.Lvl + r.Next(0, 3));
                            map[i, j].IsEnemy = true;
                            map[i, j].X = i;
                            map[i, j].Y = j;
                        }
                        else
                        {
                            map[i, j] = new Tile();
                            map[i, j].IsEnemy = false;
                            map[i, j].X = i;
                            map[i, j].Y = j;
                        }

                    }


                }

            }
            map[9, 9] = e;
            map[9, 9].IsBoss = true;
        }
        public void printTile(Tile t)
        {
            if (t.X == c.X && t.Y == c.Y)
            {
                Console.Write("| YOU |");
            }
            else if (t.X == e.X && t.Y == e.Y)
            {
                Console.Write("| BAD |");
            }
            else if (t.IsEnemy == true)
            {
                Console.Write("|  E  |");
            }
            else
            {
                Console.Write("|     |");
            }
        }
        public void printMap()
        {
            Console.Clear();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == c.X && j == c.Y)
                    {
                        Console.Write("|YOU |");
                    }
                    else if (i == e.X && j == e.Y)
                    {
                        Console.Write("|BOSS|");
                    }
                    else if (map[i, j].IsEnemy == true)
                    {
                        Console.Write("|ORC |");
                    }
                    else
                    {
                        Console.Write("|    |");
                    }




                }
                Console.WriteLine();
            }
            Console.WriteLine("\t\t'help' for list of commands\n\t\t'up','down','left','right' to move");
        }
        public void viewKnownCommands()
        {
            Console.Write("Known Commands: ");
            foreach (string cmd in c.Commands)
            {
                Console.Write("{0} ",cmd);
            }
            Console.WriteLine();
        }

        public void viewInv()
        {
            Console.Clear();
            Console.WriteLine("Inventory: ");
            foreach (Item item in c.Inv)
            {
                Console.Write("Name: {0}| {1}\n\t Value: {2} gold.\n\n ", item.iname,item.idescription,item.ivalue);
            }
            string cmd = "";
           
            Console.Write("Enter 'close' or 'use' to continue... ");
            cmd = Console.ReadLine();
            cmd.ToLower();

            if (cmd == "use")
            {
                Use();
            }
            else if (cmd == "quit")
            {
                Environment.Exit(0);
            }
            else
            {
                printMap();
            }

        }

        public void Use()
        {
            string itemName = "";
            //printMap();
            Console.Write("Enter Name of Item to Equip: ");
            itemName = Console.ReadLine();
            //itemName.ToLower();
            bool done = false;
            foreach (Item item in c.Inv)
            {
                if (itemName == item.iname)
                {
                    c.AddItemtoChar(item);
                    done = true;
                    Console.WriteLine(item.iname + " Equiped.");
                }
                if (done) break;
            }
            Console.ReadLine();
            Console.Clear();
            printMap();
        }

        public void Stats()
        {
            Console.Write("Player Stats: (lvl {0})\n\t HP: {1} \t Attack: {2}\n\t Speed: {3} \t Defense: {4}\n Total Exp: {5}\n",c.Lvl,c.HP,c.Attack,c.Speed,c.Defense,c.Exp);
        }
        public bool checkLegalMove(string d)
        {
            bool legal = false;
            switch (d)
            {
                case "up":
                    if (c.X > 0) legal = true;
                    break;
                case "down":
                    if (c.X < 9) legal = true;
                    break;
                case "left":
                    if (c.Y > 0) legal = true;
                    break;
                case "right":
                    if (c.Y < 9) legal = true;
                    break;
                default:
                    legal = false;
                    break;
            }
            return legal;
        }
        public void MoveChar(Character c,string d)
        {
            d.ToLower();
            if (checkLegalMove(d))
            {
                switch (d)
                {
                    case "up":
                        c.X--;
                        break;
                    case "down":
                        c.X++;
                        break;
                    case "left":
                        c.Y--;
                        break;
                    case "right":
                        c.Y++;
                        break;
                    default:
                        break;
                }
                
                printMap();

                if (c.X == map[c.X,c.Y].X && c.Y == map[c.X,c.Y].Y && map[c.X,c.Y].IsEnemy == true)
                {
                    map[c.X, c.Y] = map[c.X, c.Y];
                    Battle(ref c, (Enemy)map[c.X,c.Y]);
                }               
            }else if (d == "quit")
            {
                Environment.Exit(0);
            }
            else
            {
                Console.Write("Not a valid move.\n    Use: 'up', 'down', 'left', 'right', or 'quit'\n\n");
            }
        }

        public void Battle(ref Character c, Enemy t)
        {
            double dmgTaken = 0;
            double dmgTakenP = 0;
            double maxHp = 100;
            string cmd = "";
            Console.WriteLine("BATTLE!\n Character({0}): {1:F2}\n     Enemy({2}): {3:F2}", c.Lvl,c.HP,t.Lvl,t.HP);
            Console.Write("Press ENTER to fight, or type 'run' to run away. ");
            cmd = Console.ReadLine();
            cmd.ToLower();
            if (cmd == "run")
            {
                return;
            }
            cmd = "";
            while (c.Alive && t.Alive)
            {
                
                Console.Write("Health: {0:F2} |", c.HP);
                maxHp = c.HP;
                for (int i = 0; i < c.HP/10; i++)
                {
                    Console.Write("-");
                }
                Console.Write("\t\t"); 
                for (int i = 0; i < t.HP/10; i++)
                {
                    Console.Write("-");
                }
                Console.Write("| {0:F2} :Enemy",t.HP);
                Console.WriteLine("\nIt's your turn... What will you do? ");
 
                cmd = Console.ReadLine();
                printMap();
                cmd.ToLower();
                Console.WriteLine("type 'attack' to fight, or 'use potion' if available.");
                switch (cmd)
                {
                    case "attack":
                        dmgTaken = PAttack(c);
                        if (dmgTaken > t.Defense)
                        {
                            dmgTaken = dmgTaken - (t.Defense/2);
                            t.HP = t.HP - dmgTaken;
                        }
                        else dmgTaken = 0;
                        Console.WriteLine("Dealt: {0:F2} damage!",dmgTaken);
                        break;
                    case "use potion":

                        break;
                    default:
                        break;
                }
                if (t.Alive)
                {
                    dmgTakenP = EAttack(t);
                    if (dmgTakenP > c.Defense)
                    {
                        dmgTakenP = dmgTakenP - (c.Defense/2);
                        c.HP = c.HP - dmgTakenP;
                    }
                    else dmgTakenP = 0;
                    Console.WriteLine("Received: {0:F2} damage!", dmgTakenP);
                }
                
            }
            if (t.Alive != true && c.Alive && t.IsBoss == false)
            {
                c.HP = maxHp;
                t.Die(c);
                map[c.X, c.Y].IsEnemy = false;
                double preExp = c.Exp;
                int preLvl = c.Lvl;
                c.Exp += 10 * t.Lvl;
                c.Lvl = c.Lvl;
                if (preLvl < c.Lvl)
                {
                    c.LevelUp();
                }
                Console.WriteLine("Level: {0} \t Health: {1:F2}\n Exp Gained: {2:F2}", c.Lvl, c.HP,(c.Exp - preExp));
            }
            else if (t.Alive && c.Alive != true)
            {
                Console.WriteLine("You have been Killed. Type 'play' to play again, or 'quit' to quit.");
                
            }
            else if (t.Alive != true && c.Alive && t.IsBoss == true)
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\tYou've Defeated the Boss!! \n\n\t\t\tYOU WIN!!!");
                string qu = Console.ReadLine();
                Environment.Exit(0);
            }
            string con = Console.ReadLine();
            printMap();
        }
        public double PAttack(Character t)
        {
            double dmg = 0;
            dmg = (t.Attack *  r.Next(2,15));
            return dmg;
        }
        public double EAttack(Enemy t)
        {
            double dmg = 0;
            dmg = (t.Attack * r.Next(2,4));
            return dmg;
        }

    }
}
