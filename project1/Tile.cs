﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project1
{
    public class Tile
    {

        int x = 0;
        int y = 0;
        bool isenemy = false;
        bool isboss = false;

        public Tile()
        {
        }
        public bool IsEnemy
        {
            get { return isenemy; }
            set { isenemy = value; }
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public bool IsBoss
        {
            get { return isboss; }
            set { isboss = value; }
        }
    }
}
