﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project1
{
    public class Character : Tile
    {
        private double hp = 100;
        private double attack = 1;
        private int lvl = 1;
        private double defense = 1;
        private double speed = 1;
        private double exp = 0;
        public Item armor = new Item("Rags",0,"Torn Rags",1,"armor");
        public Item weapon = new Item("Fists",0,"Bare Knuckles",1,"weapon");

        private List<Item> inv = new List<Item>();
        private List<string> commands = new List<string>();
        bool alive = true;

        public Character(int lvl)
        {
            Lvl = lvl;
        }

        public List<Item> Inv
        {
            get { return inv; }
            set { inv = value; }
        }
        public List<string> Commands
        {
            get { return commands; }
            set { commands = value; }
        }
        public bool Alive
        {
            get { return alive; }
            set { alive = value; }
        }
        public double HP
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
                if (hp <= 0) { Alive = false; }
            }
        }
        public double Attack
        {
            get
            {
                return attack + weapon.iquality;
            }
            set
            {
                attack = value;
            }
        }

        public double Defense
        {
            get
            {
                return defense + armor.iquality;
            }

            set
            {
                defense = value;
            }
        }
        public double Exp
        {
            get { return exp; }
            set
            {
                exp = value;
                
            }
        }
        public int Lvl
        {
            get
            {
                return lvl;
            }
            set
            {
                lvl = ((int)Math.Floor(Exp / 100))+1;
            }
        }
        public double Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }

        public void AddItemToInv(Item i)
        {
            Inv.Add(i);
        }
        public void AddItemtoChar(Item i)
        {
            switch (i.itype)
            {
                case "weapon":
                    if (weapon.iname != "") AddItemToInv(weapon);
                    weapon = i;
                    Inv.Remove(i);
                    break;
                case "armor":
                    if (armor.iname != "") AddItemToInv(armor);
                    armor = i;
                    Inv.Remove(i);
                    break;
                case "potion":
                    HP += i.iquality * 25;
                    if (HP > 100) HP = 100;
                    Inv.Remove(i);
                    break;
            }
        }
 
        public void LevelUp()
        {
            Speed = Lvl * 2.5;
            Defense = 2 * Lvl;
            Attack = 3 * Lvl;
        }
    }
}
