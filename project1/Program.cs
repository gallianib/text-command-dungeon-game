﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project1
{
    class Program
    {
        static void Main(string[] args)
        {
            Play();
            string play = "play";
            play = Console.ReadLine();
            if (play == "play")
            {
                Console.Clear();
                Play();
            }
            else Environment.Exit(0);
        }
        static void Play()
        {
            Map map = new Map();
            map.initMap();
            map.printMap();
            //Console.WriteLine("\t\t'help' for list of commands\n\t\t'up','down','left','right' to move");
            while (map.c.Alive)
            {
                Console.Write("Command: ");
                string d = Console.ReadLine();
                d.ToLower();

                if (d == "help")
                {
                    map.viewKnownCommands();
                }
                else if (d == "inv")
                {
                    map.viewInv();
                }
                else if (d == "stats")
                {
                    map.Stats();
                }
                else if (d == "rest")
                {
                    if (map.c.HP <= 75) map.c.HP += 25;
                    else if (map.c.HP > 75 && map.c.HP < 100) map.c.HP += (100 - map.c.HP);
                    map.Stats();
                }
                else map.MoveChar(map.c, d);

            }

        }
    }
}

